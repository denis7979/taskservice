<?php

namespace KnowbaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use KnowbaseBundle\Entity\Questions;
use KnowbaseBundle\Entity\Answers;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

class MainController extends Controller
{

    private $pathTofiles = '/var/www/html/KnowBase/KnowBase/web/images/tasks/';

    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $fullname = $this->getUser()->getFirstname()." ".$this->getUser()->getSecondname();
        return $this->render('KnowbaseBundle:Home:index.html.twig', ['fullname' => $fullname]);
    }

    public function listAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {

            $questions = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->createQueryBuilder('q')
                ->where('q.account = :accountid OR q.publicAccount = :accountid')
                ->setParameter('accountid', $this->getUser()->getId())
                ->getQuery()
                ->getResult();

            $form = $this->createFormBuilder($questions)
                ->add('status', ChoiceType::class, [
                    'required' => false,
                    'label' => 'Статус',
                    'choices' => [
                        'Ожидает ответа' => Questions::STATUS_WAITING_ANSWER,
                        'Получен ответ' => Questions::STATUS_ANSWERED,
                        'Опубликован' => Questions::STATUS_PUBLISHED,
                        'Закрыт' => Questions::STATUS_CLOSED
                    ]
                ])
                ->add('question', SearchType::class, ['required' => false, 'label' => 'Поиск по ключевому слову'])
                ->add('save', SubmitType::class, ['label' => 'Получить список вопросов'])
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                if ($form->get('status')->getData()) {
                    $questions = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->createQueryBuilder('q')
                        ->where('(q.account = :accountid OR q.publicAccount = :accountid) AND q.status = :status')
                        ->setParameters(['accountid' => $this->getUser()->getId(), 'status' => $form->get('status')->getData()])
                        ->getQuery()
                        ->getResult();
                }

                if ($form->get('question')->getData()) {
                    $text = $form->get('question')->getData();
                    $questions = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->createQueryBuilder('q')
                        ->where('(q.account = :accountid OR q.publicAccount = :accountid) AND q.question LIKE :text')
                        ->setParameters(['accountid' => $this->getUser()->getId(), 'text' => "%$text%"])
                        ->getQuery()
                        ->getResult();
                }
            }

            return $this->render('KnowbaseBundle:Default:list.html.twig',
                [
                    'questions' => $questions,
                    'account_id' => $this->getUser()->getId(),
                    'form' => $form->createView(),
                    'waiting' => Questions::STATUS_WAITING_ANSWER,
                    'publish' => Questions::STATUS_PUBLISHED,
                    'answered' => Questions::STATUS_ANSWERED,
                    'closed' => Questions::STATUS_CLOSED,
                ]);
        }

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $questions = $this->getDoctrine()->getManager()
                ->getRepository('KnowbaseBundle:Questions')
                ->findBy(
                    ['status' => [Questions::STATUS_WAITING_ANSWER, Questions::STATUS_HIDDEN]],
                    ['id' => 'ASC']
                    );

            $form = $this->createFormBuilder($questions)
                ->add('status', ChoiceType::class, [
                    'required' => false,
                    'label' => 'Статус',
                    'choices' => [
                        'Ожидает ответа' => Questions::STATUS_WAITING_ANSWER,
                        'Получен ответ' => Questions::STATUS_ANSWERED,
                        'Опубликован' => Questions::STATUS_PUBLISHED,
                        'Закрыт' => Questions::STATUS_CLOSED
                    ]
                ])
                ->add('question', SearchType::class, ['required' => false, 'label' => 'Поиск по ключевому слову'])
                ->add('save', SubmitType::class, ['label' => 'Получить список вопросов'])
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                if ($form->get('status')->getData()) {
                    $status = (int)$form->get('status')->getData();
                    $questions = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->createQueryBuilder('q')
                        ->where('q.status = :status')
                        ->setParameter('status', $status)
                        ->getQuery()
                        ->getResult();
                }

                if ($form->get('question')->getData()) {
                    $text = $form->get('question')->getData();
                    $questions = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->createQueryBuilder('q')
                        ->where('q.question LIKE :text')
                        ->setParameter('text', "%$text%")
                        ->getQuery()
                        ->getResult();
                }
            }

            return $this->render('KnowbaseBundle:Default:list.html.twig',
                [
                    'questions' => $questions,
                    'account_id' => $this->getUser()->getId(),
                    'form' => $form->createView(),
                    'waiting' => Questions::STATUS_WAITING_ANSWER,
                    'publish' => Questions::STATUS_PUBLISHED,
                    'answered' => Questions::STATUS_ANSWERED,
                    'closed' => Questions::STATUS_CLOSED,
                ]);
        }

        return $this->redirectToRoute('home_action');
    }

    public function askAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $task = new Questions();

        $form = $this->createForm('KnowbaseBundle\Form\Type\SetTask', $task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form->get('Category')->getData();
            $category = $category->getName();
            $task->setUsername($this->getUser()->getUsername());
            $task->setEmail($this->getUser()->getEmail());
            $task->setAccount($this->getDoctrine()
                ->getRepository('KnowbaseBundle:Accounts')->find($this->getUser()->getId()));
            $task->setStatus(Questions::STATUS_WAITING_ANSWER);
            $task->setPublicAccount(0);

            if (!$form->get('imageFile')->getData()) {
                $task->setImageName('');
                $task->setTimeTask(new \DateTime('now'));
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            $email = \Swift_Message::newInstance()
                ->setSubject('Получен вопрос в Базе знаний!')
                ->setFrom('Knowbase@ptl.ru')
                ->setTo($this->getUser()->getEmail())
                ->setCc('pda@ptl.ru')
                ->setBody($this->renderView(
                    'KnowbaseBundle:Email:settaskemail.html.twig',
                    [
                        'firstname' => $this->getUser()->getFirstname(),
                        'secondname' => $this->getUser()->getSecondname(),
                        'region' => $this->getUser()->getRegion()->getName(),
                        'question' => $form->get('Question')->getData(),
                        'time' => new \DateTime('now'),
                        'category' => $category
                    ]
                ),
                    'text/html');

            $this->get('mailer')->send($email);

            return $this->redirectToRoute('list_action');
        }

        return $this->render('KnowbaseBundle:Form:settask.html.twig', ['form' => $form->createView(),]);
    }

    public function answerAction(Request $request, $question_id, $status)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $answer = new Answers();

        $form = $this->createForm('KnowbaseBundle\Form\Type\AnswerTask', $answer);

        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $question = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->find($question_id);

        $account = $question->getAccount()->getId();

        if ($form->isSubmitted() && $form->isValid()) {
            $answer->setQuestion($this->getDoctrine()
                ->getRepository('KnowbaseBundle:Questions')->find($question_id));
            $answer->setUsername($this->getUser()->getFirstname()." ".$this->getUser()->getSecondname());
            if (!$form->get('imageFile')->getData()) {
                $answer->setImageName('');
                $answer->setAnswerTime(new \DateTime('now'));
            }
            $answer->setAccount($this->getUser()->getId());
            $em->persist($answer);

            if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                if ($question->getStatus() == Questions::STATUS_ANSWERED) {
                    $question->setStatus(Questions::STATUS_WAITING_ANSWER);
                }
            }

            if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                $question->setStatus(Questions::STATUS_ANSWERED);
            }

            if ($form->get('closeQuestion')->getData()) {
                $question->setStatus(Questions::STATUS_CLOSED);
            }

            if ($form->get('publishQuestion')->getData()) {
                $question->setStatus(Questions::STATUS_PUBLISHED);
                $question->setAccount($this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->find($this->getUser()->getId()));
                $question->setPublicAccount($account);
            }

            $em->persist($question);
            $em->flush();

            $question = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->find($question_id);

            $email = \Swift_Message::newInstance()
                ->setSubject('Получен ответ на вопрос в Базе знаний!')
                ->setFrom('Knowbase@ptl.ru')
                ->setTo($question->getAccount()->getEmail())
                ->setCc('pda@ptl.ru')
                ->setBody($this->renderView(
                    'KnowbaseBundle:Email:answertaskemail.html.twig',
                    [
                        'firstname' => $this->getUser()->getFirstname(),
                        'secondname' => $this->getUser()->getSecondname(),
                        'region' => $this->getUser()->getRegion()->getName(),
                        'question' => $question,
                        'closed' => Questions::STATUS_CLOSED,
                        'time' => new \DateTime('now'),
                    ]
                ),
                    'text/html');

            $this->get('mailer')->send($email);

            return $this->redirectToRoute('list_action');
        }

        if ($status == 1) {
            if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                $question->setStatus(Questions::STATUS_ANSWERED);
            } else {
                if ($question->getStatus() == Questions::STATUS_ANSWERED) {
                    $question->setStatus(Questions::STATUS_WAITING_ANSWER);
                }
            }
            $em->persist($question);
            $em->flush();

            return $this->redirectToRoute('list_action');
        }

        return $this->render('KnowbaseBundle:Form:answertask.html.twig',
            [
                'form' => $form->createView(),
                'question' => $question,
                'closed' => Questions::STATUS_CLOSED,
                'account_id' => $this->getUser()->getId(),
                'waiting' => Questions::STATUS_WAITING_ANSWER,
                'answered' => Questions::STATUS_ANSWERED,
            ]);
    }

    public function listPublicAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $categories = $this->getDoctrine()->getManager()->getRepository('KnowbaseBundle:Category')->findAll();

        return $this->render('KnowbaseBundle:Default:listPublic.html.twig',
            [
                'categories' => $categories,
                'publish' => Questions::STATUS_PUBLISHED
            ]);
    }

    public function editQuestionAction(Request $request, $question_id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $question = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->find($question_id);

        $form = $this->createForm('KnowbaseBundle\Form\Type\SetTask', $question);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($question);
            $em->flush();

            return $this->redirectToRoute('list_action');
        }

        return $this->render('KnowbaseBundle:Form:settask.html.twig', ['form' => $form->createView()]);
    }

    public function removeQuestionAction($question_id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $fs = new Filesystem();

        $question = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->find($question_id);

        $file = $question->getImageName();

        if ($file !== 'null' && $file !== null && $file !== '') {
            $fs->remove($this->pathTofiles.$file);
        }

        $repository = $this->getDoctrine()->getRepository('KnowbaseBundle:Answers');

        $query = $repository->createQueryBuilder('a')
            ->where('a.question = :question_id AND NOT a.imageName = \'null\'')
            ->setParameter('question_id', $question_id)
            ->getQuery();

        $result = $query->getResult();

        foreach ($result as $answer) {
            $file = $answer->getImageName();
            if ($file !== 'null' && $file !== null && $file !== '') {
                $fs->remove($this->pathTofiles.$file);
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($question);
        $em->flush();

        return $this->redirectToRoute('list_action');
    }

    public function editAnswerAction(Request $request, $answer_id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $answer = $this->getDoctrine()->getRepository('KnowbaseBundle:Answers')->find($answer_id);
        $question = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->find($answer->getQuestion());
        $question_id = $question->getId();

        $form = $this->createForm('KnowbaseBundle\Form\Type\AnswerTask', $answer);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('closeQuestion')->getData()) {
                $question->setStatus(Questions::STATUS_CLOSED);
            }
            if ($form->get('publishQuestion')->getData()) {
                $question->setStatus(Questions::STATUS_PUBLISHED);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($answer);
            $em->persist($question);
            $em->flush();

            return $this->redirectToRoute('answer_action', ['question_id' => $question_id, 'status' => 0]);
        }

        return $this->render('KnowbaseBundle:Form:answertask.html.twig', ['form' => $form->createView(), 'editanswer' => 1]);
    }

    public function removeAnswerAction($question_id, $answer_id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $fs = new Filesystem();

        $answer = $this->getDoctrine()->getRepository('KnowbaseBundle:Answers')->find($answer_id);

        $file = $answer->getImageName();

        if ($file !== 'null' && $file !== null && $file !== '') {
            $fs->remove($this->pathTofiles.$file);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($answer);
        $em->flush();

        return $this->redirectToRoute('answer_action', ['question_id' => $question_id, 'status' => 0]);
    }
}