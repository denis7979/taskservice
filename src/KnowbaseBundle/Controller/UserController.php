<?php

namespace KnowbaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use KnowbaseBundle\Entity\Accounts;
use KnowbaseBundle\Entity\Questions;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;

class UserController extends Controller
{

    private $pathTofiles = '/var/www/html/KnowBase/KnowBase/web/images/tasks/';

    public function indexAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Доступ запрещен!');
        
        $account = new Accounts();

        $form = $this->createForm('KnowbaseBundle\Form\Type\RegistrationUser', $account);

        $form1 = $this->createForm('KnowbaseBundle\Form\Type\SortUsers', $account, ['action' => $this->generateUrl('users_sort_action')]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($account->getPassword() !== $account->getPlainPassword()) {
                return $this->render('KnowbaseBundle:Form:users.html.twig', ['form' => $form->createView(), 'errorpass' => 'Ошибка в подтверждении пароля!']);
            }

            if ($this->getDoctrine()->getManager()->getRepository('KnowbaseBundle:Accounts')->findBy(['username' => $form->get('username')->getData()])) {
                return $this->render('KnowbaseBundle:Form:users.html.twig', ['form' => $form->createView(), 'errorpass' => 'Логин уже существует!']);
            }

            if ($this->getDoctrine()->getManager()->getRepository('KnowbaseBundle:Accounts')->findBy(['email' => $form->get('email')->getData()])) {
                return $this->render('KnowbaseBundle:Form:users.html.twig', ['form' => $form->createView(), 'errorpass' => 'Адрес электронной почты уже существует!']);
            }

            if (!preg_match('/^[A-Za-z0-9.]+\@ptl.ru$/i', $form->get('email')->getData())) {
                return $this->render('KnowbaseBundle:Form:users.html.twig', ['form' => $form->createView(), 'errorpass' => 'Адрес электронной почты должен быть в домене ptl.ru!']);
            }

            $account->setEnabled(true);

            $em = $this->getDoctrine()->getManager();
            $em->persist($account);
            $em->flush();

            $email = \Swift_Message::newInstance()
                ->setSubject('Доступ в Базу знаний!')
                ->setFrom('Knowbase@ptl.ru')
                ->setTo($form->get('email')->getData())
                ->setCc('pda@ptl.ru')
                ->setBody($this->renderView(
                    'KnowbaseBundle:Email:registration.html.twig',
                    [
                        'firstname' => $form->get('firstname')->getData(),
                        'secondname' => $form->get('secondname')->getData(),
                        'username' => $form->get('username')->getData(),
                        'password' => $form->get('password')->getData()
                    ]
                ));

            $this->get('mailer')->send($email);
        }

        $accounts = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->findAll();

        return $this->render('KnowbaseBundle:Form:users.html.twig', ['form' => $form->createView(), 'form1' => $form1->createView(), 'accounts' => $accounts]);
    }

    public function blockAction($account_id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Доступ запрещен!');

        $account = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->find($account_id);
        
        $account->setEnabled(false);

        $em = $this->getDoctrine()->getManager();
        $em->persist($account);
        $em->flush();

        return $this->redirectToRoute('users_public_action');
    }

    public function unblockAction($account_id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Доступ запрещен!');

        $account = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->find($account_id);

        $account->setEnabled(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($account);
        $em->flush();

        return $this->redirectToRoute('users_public_action');

    }

    public function removeAction($account_id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Доступ запрещен!');

        $fs = new Filesystem();

        $account = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->find($account_id);

        $repository = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions');

        $query = $repository->createQueryBuilder('q')
            ->where('q.account = :accountid')
            ->setParameter('accountid', $account_id)
            ->getQuery();

        $result = $query->getResult();

        foreach ($result as $question) {
            $file = $question->getImageName();
            $question_id = $question->getId();
            if ($file !== 'null' && $file !== null && $file !== '') {
                $fs->remove($this->pathTofiles.$file);
            }

            $repository = $this->getDoctrine()->getRepository('KnowbaseBundle:Answers');

            $query = $repository->createQueryBuilder('a')
                ->where('a.question = :question_id')
                ->setParameter('question_id', $question_id)
                ->getQuery();

            $result = $query->getResult();

            foreach ($result as $answer) {
                $file = $answer->getImageName();
                if ($file !== 'null' && $file !== null && $file !== '') {
                    $fs->remove($this->pathTofiles.$file);
                }
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($account);
        $em->flush();


        return $this->redirectToRoute('users_public_action');
    }

    public function listAction(Request $request, $account_id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Доступ запрещен!');

        $questions = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->createQueryBuilder('q')
            ->where('(q.account = :accountid OR q.publicAccount = :accountid)')
            ->setParameter('accountid', $account_id)
            ->orderBy('q.id', 'DESC')
            ->getQuery()
            ->getResult();

        $form = $this->createFormBuilder($questions)
            ->add('status', ChoiceType::class, [
                'required' => false,
                'label' => 'Статус',
                'choices' => [
                    'Ожидает ответа' => Questions::STATUS_WAITING_ANSWER,
                    'Получен ответ' => Questions::STATUS_ANSWERED,
                    'Опубликован' => Questions::STATUS_PUBLISHED,
                    'Закрыт' => Questions::STATUS_CLOSED
                ]
            ])
            ->add('question', SearchType::class, ['required' => false, 'label' => 'Поиск по ключевому слову'])
            ->add('save', SubmitType::class, ['label' => 'Получить список вопросов'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('status')->getData()) {
                $questions = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->createQueryBuilder('q')
                    ->where('(q.account = :accountid OR q.publicAccount = :accountid) AND q.status = :status')
                    ->setParameters(['accountid' => $account_id, 'status' => $form->get('status')->getData()])
                    ->orderBy('q.id', 'DESC')
                    ->getQuery()
                    ->getResult();
            }

            if ($form->get('question')->getData()) {
                $text = $form->get('question')->getData();
                $questions = $this->getDoctrine()->getRepository('KnowbaseBundle:Questions')->createQueryBuilder('q')
                    ->where('(q.account = :accountid OR q.publicAccount = :accountid) AND q.question LIKE :text')
                    ->setParameters(['accountid' => $account_id, 'text' => "%$text%"])
                    ->getQuery()
                    ->getResult();
            }
        }

        return $this->render('KnowbaseBundle:Default:list.html.twig',
            [
                'questions' => $questions,
                'form' => $form->createView(),
                'waiting' => Questions::STATUS_WAITING_ANSWER,
                'publish' => Questions::STATUS_PUBLISHED,
                'answered' => Questions::STATUS_ANSWERED,
                'closed' => Questions::STATUS_CLOSED,
            ]);
    }

    public function passwordAction(Request $request, $account_id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Доступ запрещен!');

        $account = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->find($account_id);

        $form = $this->createForm('KnowbaseBundle\Form\Type\ChangePassword', $account);

        $form1 = $this->createForm('KnowbaseBundle\Form\Type\ChangeProfile', $account);

        $form->handleRequest($request);

        $form1->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($account->getPassword() !== $account->getPlainPassword()) {
                return $this->render('KnowbaseBundle:Form:userpassword.html.twig', ['form' => $form->createView(), 'form1' => $form1->createView(), 'errorpass' => 'Ошибка в подтверждении пароля!']);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($account);
            $em->flush();

            return $this->redirectToRoute('users_public_action');
        }

        if ($form1->isSubmitted() && $form1->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($account);
            $em->flush();

            return $this->redirectToRoute('users_public_action');
        }

        return $this->render('KnowbaseBundle:Form:userpassword.html.twig', ['form' => $form->createView(), 'form1' => $form1->createView()]);
    }

    public function profileAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $account = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->find($this->getUser()->getId());

        $form = $this->createForm('KnowbaseBundle\Form\Type\ChangePassword', $account);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($account->getPassword() !== $account->getPlainPassword()) {
                return $this->render('KnowbaseBundle:Form:userpassword.html.twig', ['form' => $form->createView(), 'errorpass' => 'Ошибка в подтверждении пароля!']);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($account);
            $em->flush();

            return $this->redirectToRoute('home_action');
        }

        return $this->render('KnowbaseBundle:Form:userpassword.html.twig', ['form' => $form->createView()]);
    }

    public function sortAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Доступ запрещен!');

        $account = new Accounts();

        $form = $this->createForm('KnowbaseBundle\Form\Type\SortUsers', $account);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $secondname = $form->get('secondname')->getData();
            $sortStatus = $form->get('sortStatus')->getData();
            $timeLogin = $form->get('sortTimeLogin')->getData();
            $longTimeLogin = $form->get('sortLongTimeWithoutLogin')->getData();
            $region = $form->get('region')->getData();

            if ($sortStatus) {
                $accounts = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->createQueryBuilder('a')
                    ->where('a.enabled = 0')
                    ->orderBy('a.secondname', 'DESC')
                    ->getQuery()
                    ->getResult();
            }

            if ($secondname) {
                $accounts = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->createQueryBuilder('a')
                    ->where('a.secondname LIKE :name')
                    ->setParameter('name', "%$secondname%")
                    ->getQuery()
                    ->getResult();
            }

            if ($timeLogin) {
                $accounts = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->createQueryBuilder('a')
                    ->orderBy('a.lastLogin', 'ASC')
                    ->getQuery()
                    ->getResult();
            }

            if ($longTimeLogin && !$region) {
                $accounts = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->findAll();
                return $this->render('KnowbaseBundle:Form:usersLongLogin.html.twig', ['accounts' => $accounts]);
            }

            if ($region) {
                $accounts = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->createQueryBuilder('a')
                    ->where('a.region = :id')
                    ->setParameter('id', $region)
                    ->getQuery()
                    ->getResult();
            }

            if ($region && $secondname) {
                $accounts = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->createQueryBuilder('a')
                    ->where('a.region = :id AND a.secondname LIKE :name')
                    ->setParameters(['id' => $region, 'name' => "%$secondname%"])
                    ->getQuery()
                    ->getResult();
            }

            if ($region && $sortStatus) {
                $accounts = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->createQueryBuilder('a')
                    ->where('a.region = :id AND a.enabled = 0')
                    ->orderBy('a.secondname', 'DESC')
                    ->setParameter('id', $region)
                    ->getQuery()
                    ->getResult();
            }

            if ($region && $timeLogin) {
                $accounts = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->createQueryBuilder('a')
                    ->where('a.region = :id')
                    ->orderBy('a.lastLogin', 'ASC')
                    ->setParameter('id', $region)
                    ->getQuery()
                    ->getResult();
            }

            if ($region && $longTimeLogin) {
                $accounts = $this->getDoctrine()->getRepository('KnowbaseBundle:Accounts')->createQueryBuilder('a')
                    ->where('a.region = :id')
                    ->setParameter('id', $region)
                    ->getQuery()
                    ->getResult();

                return $this->render('KnowbaseBundle:Form:usersLongLogin.html.twig', ['accounts' => $accounts]);
            }
        }
        return $this->render('KnowbaseBundle:Form:users.html.twig', ['accounts' => $accounts]);
    }
}