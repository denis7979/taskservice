<?php

namespace KnowbaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AnswerTask extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Answer', TextareaType::class, ['label' => 'Написать сообщение'])
            ->add('imageFile', VichImageType::class, ['required' => false, 'label' => 'Загрузить приложение'])
            ->add('closeQuestion', CheckboxType::class, ['required' => false, 'label' => 'Закрыть вопрос'])
            ->add('publishQuestion', CheckboxType::class, ['required' => false, 'label' => 'Опубликовать вопрос'])
            ->add('Save', SubmitType::class, ['label' => 'Отправить']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'KnowbaseBundle\Entity\Answers',
        ));
    }
}