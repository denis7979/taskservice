<?php

namespace KnowbaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManager;
use KnowbaseBundle\Entity\Region;

class SortUsers extends AbstractType
{

    private $doctrine;

    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('secondname', TextType::class, ['required' => false, 'label' => 'Фамилия'])
            ->add('region', ChoiceType::class, [
                'choices' => $this->getRegion(),
                'choice_label' => function($region, $key, $index) {
                    /** @var Region $region */
                    return $region->getName();
                },
                'label' => 'Регион',
                'required' => false
            ])
            ->add('sortStatus', CheckboxType::class, ['required' => false, 'label' => 'Получить список заблокированных пользователей'])
            ->add('sortTimeLogin', CheckboxType::class, ['required' => false, 'label' => 'Сортировать по времени последней регистрации'])
            ->add('sortLongTimeWithoutLogin', CheckboxType::class, ['required' => false, 'label' => 'Получить список неактивных пользователей (более 45 дней)'])
            ->add('Save', SubmitType::class, ['label' => 'Получить']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'KnowbaseBundle\Entity\Accounts']);
    }

    public function getRegion()
    {
        $regions = $this->doctrine->getRepository('KnowbaseBundle:Region')->createQueryBuilder('r')
            ->getQuery()
            ->getResult();

        return $regions;
    }
}