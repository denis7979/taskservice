<?php
namespace KnowbaseBundle\Form\Type;

use KnowbaseBundle\Entity\Region;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationUser extends AbstractType
{
    private $doctrine;

    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, ['required' => true, 'label' => 'Имя' ])
            ->add('secondname', TextType::class, ['required' => true, 'label' => 'Фамилия' ])
            ->add('region', ChoiceType::class, [
                'choices' => $this->getRegion(),
                'choice_label' => function($region, $key, $index) {
                    /** @var Region $region */
                    return $region->getName();
                },
                'label' => 'Регион',
                'required' => true
            ])
            ->add('username', TextType::class, ['required' => true, 'label' => 'Логин'])
            ->add('password', PasswordType::class, ['required' => true, 'label' => 'Пароль'])
            ->add('plainPassword', PasswordType::class, ['required' => true, 'label' => 'Подтверждение пароля'])
            ->add('email', EmailType::class, ['required' => true, 'label' => 'Email'])
            ->add('Save', SubmitType::class, ['label' => 'Зарегистрировать']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'KnowbaseBundle\Entity\Accounts']);
    }

    public function getRegion()
    {
        $regions = $this->doctrine->getRepository('KnowbaseBundle:Region')->findAll();

        return $regions;
    }
}