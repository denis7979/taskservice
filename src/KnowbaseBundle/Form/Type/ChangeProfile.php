<?php

namespace KnowbaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use KnowbaseBundle\Entity\Region;
use Doctrine\ORM\EntityManager;

class ChangeProfile extends AbstractType
{

    private $doctrine;

    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, ['required' => false, 'label' => 'Имя'])
            ->add('secondname', TextType::class, ['required' => false, 'label' => 'Фамилия'])
            ->add('region', ChoiceType::class, [
                'choices' => $this->getRegion(),
                'choice_label' => function($region, $key, $index) {
                    /** @var Region $region */
                    return $region->getName();
                },
                'label' => 'Регион',
                'required' => false
            ])
            ->add('username', TextType::class, ['required' => false, 'label' => 'Логин'])
            ->add('email', EmailType::class, ['required' => false, 'label' => 'Фамилия'])
            ->add('Save', SubmitType::class, ['label' => 'Изменить данные']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'KnowbaseBundle\Entity\Accounts']);
    }

    public function getRegion()
    {
        $regions = $this->doctrine->getRepository('KnowbaseBundle:Region')->createQueryBuilder('r')
            ->getQuery()
            ->getResult();

        return $regions;
    }
}