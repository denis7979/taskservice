<?php

namespace KnowbaseBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\FormBuilderInterface;
use KnowbaseBundle\Entity\Category;

class SetTask extends AbstractType
{
    private $doctrine;

    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Category', ChoiceType::class, [
                'choices' => $this->getCategory(),
                'choice_label' => function($category, $key, $index) {
                    /** @var Category $category */
                    return strtoupper($category->getName());
                },
                'label' => 'Тема'
            ])
            ->add('Question', TextareaType::class, ['label' => 'Вопрос'])
            ->add('imageFile', VichImageType::class, ['required' => false, 'label' => 'Загрузить приложение'])
            ->add('Save', SubmitType::class, ['label' => 'Отправить']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'KnowbaseBundle\Entity\Questions']);
    }

    public function getCategory()
    {
        $categories = $this->doctrine->getRepository('KnowbaseBundle:Category')->findAll();

        return $categories;
    }
}