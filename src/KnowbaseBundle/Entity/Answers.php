<?php

namespace KnowbaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Answers
 *
 * @ORM\Table(name="answers")
 * @ORM\Entity(repositoryClass="KnowbaseBundle\Repository\AnswersRepository")
 * @Vich\Uploadable
 */
class Answers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Questions", inversedBy="answers")
     * @ORM\JoinColumn(name="Question_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $question;

    /**
     * @var string
     *
     * @ORM\Column(name="Answer", type="text")
     * @Assert\NotBlank()
     */
    private $answer;

    /**
     * @var string
     *
     * @ORM\Column(name="Username", type="string", length=255)
     */
    private $username;

    /**
     * @var int
     *
     * @ORM\Column(name="Account_id", type="integer")
     */
    private $account;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="AnswerTime", type="datetime")
     */
    private $answerTime;

    /**
     * @Assert\File(
     *     mimeTypes = {"application/pdf", "image/jpeg", "image/gif", "image/tiff"},
     *     maxSize = "2M",
     *     maxSizeMessage = "Превышен размер загружаемого файла. Максимальный размер 2М.",
     *     mimeTypesMessage = "Неверный формат файла. Доступные форматы: pdf, jpeg, gif, tiff"
     *     )
     * @Vich\UploadableField(mapping="task_image", fileNameProperty="imageName")
     * @var File $imageFile
     */
    private $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="ImageName", type="string", length=255)
     *
     */
    private $imageName;

    /**
     * @var boolean
     */
    private $closeQuestion;

    /**
     * @var boolean
     */
    private $publishQuestion;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return Answers
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Answers
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set answerTime
     *
     * @param \DateTime $answerTime
     *
     * @return Answers
     */
    public function setAnswerTime($answerTime)
    {
        $this->answerTime = $answerTime;

        return $this;
    }

    /**
     * Get answerTime
     *
     * @return \DateTime
     */
    public function getAnswerTime()
    {
        return $this->answerTime;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     * @return Answers
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->answerTime = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     *
     * @return Answers
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set questionId
     *
     * @param \KnowbaseBundle\Entity\Questions $questionId
     *
     * @return Answers
     */
    public function setQuestion(\KnowbaseBundle\Entity\Questions $questionId = null)
    {
        $this->question = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return \KnowbaseBundle\Entity\Questions
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @return boolean
     */
    public function getCloseQuestion()
    {
        return $this->closeQuestion;
    }

    /**
     * @param boolean $close
     * @return boolean
     */
    public function setCloseQuestion($close)
    {
        $this->closeQuestion = $close;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getPublishQuestion()
    {
        return $this->publishQuestion;
    }

    /**
     * @param boolean $publish
     * @return boolean
     */
    public function setPublishQuestion($publish)
    {
        $this->publishQuestion = $publish;

        return $this;
    }

    /**
     * Set account
     *
     * @param integer $account
     *
     * @return Answers
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return integer
     */
    public function getAccount()
    {
        return $this->account;
    }
}
