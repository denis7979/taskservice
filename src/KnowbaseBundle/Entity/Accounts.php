<?php

namespace KnowbaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Accounts
 *
 * @ORM\Table(name="accounts")
 * @ORM\Entity(repositoryClass="KnowbaseBundle\Repository\AccountsRepository")
 */
class Accounts extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Questions", mappedBy="accounts")
     */
    protected $questions;

    /**
     * @ORM\Column(name = "firstname", type="string")
     * @Assert\NotBlank(groups={"Registration", "Profile"})
     */
    protected $firstname;

    /**
     * @ORM\Column(name = "secondname", type="string")
     * @Assert\NotBlank(groups={"Registration", "Profile"})
     */
    protected $secondname;

    /**
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="accounts")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * @Assert\NotBlank(groups={"Registration", "Profile"})
     */
    protected $region;

    /**
     * @var boolean
     */
    protected $sortStatus;

    /**
     * @var boolean
     */
    protected $sortTimeLogin;

    /**
     * @var boolean
     */
    protected $sortLongTimeWithoutLogin;

    public function __construct()
    {
        parent::__construct();
        $this->questions = new ArrayCollection();
    }

    /**
     * Add question
     *
     * @param \KnowbaseBundle\Entity\Questions $question
     *
     * @return Accounts
     */
    public function addQuestion(\KnowbaseBundle\Entity\Questions $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \KnowbaseBundle\Entity\Questions $question
     */
    public function removeQuestion(\KnowbaseBundle\Entity\Questions $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Get Account id
     * @return \KnowbaseBundle\Entity\Accounts
     */
    public function getAccount()
    {
        return $this->getId();
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Accounts
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set secondname
     *
     * @param string $secondname
     *
     * @return Accounts
     */
    public function setSecondname($secondname)
    {
        $this->secondname = $secondname;

        return $this;
    }

    /**
     * Get secondname
     *
     * @return string
     */
    public function getSecondname()
    {
        return $this->secondname;
    }

    /**
     * Set region
     *
     * @param \KnowbaseBundle\Entity\Region $region
     *
     * @return Accounts
     */
    public function setRegion(\KnowbaseBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \KnowbaseBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    public function getSortStatus()
    {
        return $this->sortStatus;
    }

    public function setSortStatus($sort)
    {
        $this->sortStatus = $sort;

        return $this;
    }

    public function getSortTimeLogin()
    {
        return $this->sortTimeLogin;
    }

    public function setSortTimeLogin($sort)
    {
        $this->sortTimeLogin = $sort;

        return $this;
    }

    public function getSortLongTimeWithoutLogin()
    {
        return $this->sortLongTimeWithoutLogin;
    }

    public function setSortLongTimeWithoutLogin($sort)
    {
        $this->sortLongTimeWithoutLogin = $sort;

        return $this;
    }
}
