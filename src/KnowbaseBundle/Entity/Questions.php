<?php

namespace KnowbaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Questions
 *
 * @ORM\Table(name="questions")
 * @ORM\Entity(repositoryClass="KnowbaseBundle\Repository\QuestionsRepository")
 * @Vich\Uploadable
 */
class Questions
{
    const STATUS_WAITING_ANSWER = 1;
    const STATUS_PUBLISHED = 2;
    const STATUS_HIDDEN = 3;
    const STATUS_ANSWERED = 4;
    const STATUS_CLOSED = 5;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Question", type="text")
     * @Assert\NotBlank()
     */
    private $question;

    /**
     * @ORM\OneToMany(targetEntity="Answers", mappedBy="question")
     */
    private $answers;

    /**
     * @var string
     *
     * @ORM\Column(name="Username", type="string", length=255)
     *
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=255)
     *
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="Status", type="integer")
     *
     */
    private $status;

    /**
     * @Assert\File(
     *     mimeTypes = {"application/pdf", "image/jpeg", "image/gif", "image/tiff"},
     *     maxSize = "10M",
     *     maxSizeMessage = "Превышен размер загружаемого файла. Максимальный размер 2М.",
     *     mimeTypesMessage = "Неверный формат файла. Доступные форматы: pdf, jpeg, gif, tiff"
     *     )
     * @Vich\UploadableField(mapping="task_image", fileNameProperty="imageName")
     * @var File $imageFile
     */
    private $imageFile;

    /**
     * @ORM\Column(name="ImageName", type="string", length=255)
     * @var string
     *
     */
    private $imageName;

    /**
     * @ORM\Column(name="TimeTask", type="datetime")
     * @var \DateTime
     *
     */
    private $timeTask;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="questions")
     * @ORM\JoinColumn(name="Category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="Accounts", inversedBy="questions")
     * @ORM\JoinColumn(name="Account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $account;

    /**
     * @ORM\Column(name="Public_Account_id", type="integer")
     * @var int
     *
     */
    private $publicAccount;


    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return Questions
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

     /**
     * Set username
     *
     * @param string $username
     *
     * @return Questions
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Questions
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Questions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     * @return Questions
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->timeTask = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     * @return Questions
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @return string
     */
    public function setTimeTask(\DateTime $time = null)
    {
        $this->timeTask = $time;

        return $this;
    }

    /**
     * Get timeTask
     *
     * @return \DateTime
     */
    public function getTimeTask()
    {
        return $this->timeTask;
    }

    /**
     * Set category
     *
     * @param \KnowbaseBundle\Entity\Category $category
     *
     * @return Questions
     */
    public function setCategory(\KnowbaseBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \KnowbaseBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set account
     *
     * @param \KnowbaseBundle\Entity\Accounts $account
     *
     * @return Questions
     */
    public function setAccount(\KnowbaseBundle\Entity\Accounts $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \KnowbaseBundle\Entity\Accounts
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Add answer
     *
     * @param \KnowbaseBundle\Entity\Answers $answer
     *
     * @return Questions
     */
    public function addAnswer(\KnowbaseBundle\Entity\Answers $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \KnowbaseBundle\Entity\Answers $answer
     */
    public function removeAnswer(\KnowbaseBundle\Entity\Answers $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Set publicAccount
     *
     * @param integer $publicAccount
     *
     * @return Questions
     */
    public function setPublicAccount($publicAccount)
    {
        $this->publicAccount = $publicAccount;

        return $this;
    }

    /**
     * Get publicAccount
     *
     * @return integer
     */
    public function getPublicAccount()
    {
        return $this->publicAccount;
    }
}
